import React, {ReactNode} from 'react';
import {Layout as AntLayout, Menu, Button, Row, Col} from 'antd';
import {Link} from "react-router-dom";
import styled from 'styled-components'
import {isLoggedIn, setIsLoggedIn} from '../reducers/auth';
import {useSelector, useDispatch} from "react-redux";

import {useHistory} from 'react-router-dom';

const {Header, Content} = AntLayout;

const StyledLayout = styled(AntLayout)`
    min-height: 100vh;
`;

const StyledContent = styled(Content)`
    display: flex;
    justify-content: center;
    align-items: center;
`;

type Props = {
    children: ReactNode
}

function Layout({children}: Props) {
    const isAuthenticated = useSelector(isLoggedIn);
    const dispatch = useDispatch()
    const history = useHistory();

    const onSignOut = () => {
        localStorage.removeItem('access_token');

        dispatch(setIsLoggedIn(false));
        history.push('/');
    }

    return (
        <StyledLayout className="layout">
            <Header>
                <Row>
                    <Col span={23}>
                        <div className="logo"/>
                        {!isAuthenticated ?
                            <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
                                <Menu.Item key="1">
                                    <Link to="/">
                                        Home
                                    </Link>
                                </Menu.Item>
                                <Menu.Item key="2">
                                    <Link to="/login">
                                        Login
                                    </Link>
                                </Menu.Item>
                                <Menu.Item key="3">
                                    <Link to="/register">
                                        Register
                                    </Link>
                                </Menu.Item>
                            </Menu>
                            :
                            <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
                                <Menu.Item key="1">
                                    <Link to="/">
                                        Home
                                    </Link>
                                </Menu.Item>
                                <Menu.Item key="2">
                                    <Link to="/add-resume">
                                        Create Resume
                                    </Link>
                                </Menu.Item>
                                <Menu.Item key="3">
                                    <Link to="/all-resumes">
                                        My Resumes
                                    </Link>
                                </Menu.Item>
                            </Menu>
                        }
                    </Col>
                    <Col span={1}>
                        {isAuthenticated && (
                            <Button type="primary" onClick={onSignOut}>
                                Sign Out
                            </Button>
                        )}
                    </Col>
                </Row>
            </Header>
            <StyledContent style={{padding: '0 50px'}}>
                <div className="site-layout-content">
                    {children}
                </div>
            </StyledContent>
        </StyledLayout>
    )
}

export default Layout;

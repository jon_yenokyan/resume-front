import React, {useState} from 'react';

import {
    Form,
    Select,
    Input,
    Button,
    Upload,
} from 'antd';
import {UploadOutlined} from '@ant-design/icons';

import {Store} from 'rc-field-form/lib/interface';
import {VisibleScreen} from '../..';

type Props = {
    visible: number;
    onSetFormValues: (values: Store) => void;
}

function FirsStep({visible, onSetFormValues}: Props) {
    const {TextArea} = Input;
    const {Option} = Select;
    const formItemLayout = {
        labelCol: {span: 6},
        wrapperCol: {span: 18},
    };

    const normFile = (e: any) => {

        if (Array.isArray(e)) {
            return e;
        }

        if(!e.fileList.length) {
            return [];
        }

        const lastIndex = e.fileList?.length - 1;

        return e && [e.fileList[lastIndex]];
    };

    if (visible !== VisibleScreen.first) {
        return null;
    }

    return (
        <Form
            name="resume-info"
            style={{minWidth: 500, marginTop: 20}}
            {...formItemLayout}
            onFinish={onSetFormValues}
            initialValues={{
                'input-number': 3,
                'checkbox-group': ['A', 'B'],
                rate: 3.5,
            }}
        >

            <Form.Item
                name="file"
                label="Avatar"
                valuePropName="fileList"
                getValueFromEvent={normFile}
                rules={[
                    {required: true, message: 'Please Add Image'},
                ]}

            >
                <Upload  multiple={false} name="logo" listType="picture" beforeUpload={() => false}>
                    <Button>
                        <UploadOutlined/> Click to upload
                    </Button>
                </Upload>
            </Form.Item>

            <Form.Item
                label="First Name"
                name="first_name"
                rules={[
                    {required: true, message: 'Please input your first name!'},
                    {min: 4, message: 'Minimum 4 characters.'},
                ]}
            >
                <Input/>
            </Form.Item>

            <Form.Item
                label="Last Name"
                name="last_name"
                rules={[
                    {required: true, message: 'Please input your last name!'},
                    {min: 4, message: 'Minimum 4 characters.'},
                ]}
            >
                <Input/>
            </Form.Item>

            <Form.Item
                label="City"
                name="city"
                rules={[
                    {required: true, message: 'Please input your city!'},
                    {min: 4, message: 'Minimum 4 characters.'},
                ]}
            >
                <Input/>
            </Form.Item>

            <Form.Item
                label="Address"
                name="address"
                rules={[
                    {required: true, message: 'Please input your address!'},
                    {min: 4, message: 'Minimum 4 characters.'},
                ]}
            >
                <Input/>
            </Form.Item>

            <Form.Item
                label="Phone"
                name="phone"
                rules={[
                    {required: true, message: 'Please input your phone!'},
                    {min: 8, message: 'Minimum 8 characters.'},
                ]}
            >
                <Input/>
            </Form.Item>

            <Form.Item
                name="armenian"
                label="Armenian"
                hasFeedback
                rules={[{required: true, message: 'Please select Armenian level!'}]}
            >
                <Select placeholder="Please select a level">
                    <Option value="0">0</Option>
                    <Option value="1">1</Option>
                    <Option value="2">2</Option>
                    <Option value="3">3</Option>
                    <Option value="4">4</Option>
                    <Option value="5">5</Option>
                </Select>
            </Form.Item>

            <Form.Item
                name="russian"
                label="Russian"
                hasFeedback
                rules={[{required: true, message: 'Please select Russian level!'}]}
            >
                <Select placeholder="Please select a level">
                    <Option value="0">0</Option>
                    <Option value="1">1</Option>
                    <Option value="2">2</Option>
                    <Option value="3">3</Option>
                    <Option value="4">4</Option>
                    <Option value="5">5</Option>
                </Select>
            </Form.Item>
            <Form.Item
                name="english"
                label="English"
                hasFeedback
                rules={[{required: true, message: 'Please select English level!'}]}
            >
                <Select placeholder="Please select a level">
                    <Option value="0">0</Option>
                    <Option value="1">1</Option>
                    <Option value="2">2</Option>
                    <Option value="3">3</Option>
                    <Option value="4">4</Option>
                    <Option value="5">5</Option>
                </Select>
            </Form.Item>
            <Form.Item
                label="Description"
                name="description"
            >
                <TextArea/>
            </Form.Item>
            <Form.Item
                label="FB Link"
                name="fb_link"
            >
                <Input/>
            </Form.Item>
            <Form.Item
                label="Linkedin Link"
                name="linkedin_link"
            >
                <Input/>
            </Form.Item>
            <Form.Item
                label="Telegram"
                name="telegram"
            >
                <Input/>
            </Form.Item>
            <Form.Item wrapperCol={{span: 12, offset: 6}}>
                <Button type="primary" htmlType="submit">
                    Next
                </Button>
            </Form.Item>
        </Form>
    );
}

export default FirsStep;

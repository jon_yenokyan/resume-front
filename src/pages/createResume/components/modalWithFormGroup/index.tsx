import React, {useRef} from 'react';
import {Form, Input, Modal, DatePicker} from "antd";
import {Store} from 'rc-field-form/lib/interface';
type ModalProps = {
    title: string;
    name: string;
    position?: string;
    fakultet?: string;
}


const dateFormat = 'YYYY/MM/DD';

const {useForm} = Form;

const {RangePicker} = DatePicker;

type Props = {
    ModalProps: ModalProps,
    visible: boolean;
    onClose: () => void;
    onFinish: (data: Store) => void
}

function ModalWithFormGroup({ModalProps, visible, onClose, onFinish}: Props) {
    const [form] = useForm();
    const formRef = useRef(null);
    const {title, name, fakultet, position} = ModalProps

    const handleClose = () => {
        // @ts-ignore
        formRef.current.resetFields();
        onClose()
    }

    return (

        <Modal
            title={title}
            visible={visible}
            onOk={() => form.submit()}
            onCancel={handleClose}
        >
            <Form
                ref={formRef}
                name="education"
                onFinish={(values) => {
                    onFinish(values);
                    // @ts-ignore
                    formRef.current.resetFields();
                    onClose();
                }}
                form={form}
            >
                <Form.Item
                    name="name"
                    rules={[
                        {required: true},
                    ]}
                >
                    <Input size="large" placeholder={name}/>
                </Form.Item>
                {fakultet && <Form.Item
                  name="fakultet"
                  rules={[
                      {required: true},
                  ]}
                >
                  <Input size="large" placeholder={fakultet}/>
                </Form.Item>}
                {position &&
                <Form.Item
                  name="position"
                  rules={[
                      {required: true},
                  ]}
                >
                  <Input size="large" placeholder={position}/>
                </Form.Item>
                }
                <Form.Item
                    name="address"
                    rules={[
                        {required: true},
                    ]}
                >
                    <Input size="large" placeholder="Address"/>
                </Form.Item>
                <Form.Item
                    name="date"
                    rules={[
                        {required: true},
                    ]}
                >
                    <RangePicker
                        style={{width: '100%'}}
                        size="large"
                        format={dateFormat}
                    />
                </Form.Item>
            </Form>
        </Modal>
    );
}

export default ModalWithFormGroup;

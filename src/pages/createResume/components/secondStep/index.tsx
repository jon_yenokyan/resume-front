import React, {useState, ChangeEvent} from 'react';
import {Button, Input, Tag, Row, Col, message, Space} from 'antd';
import {Store} from 'rc-field-form/lib/interface';
import {VisibleScreen} from '../../index';
import {TweenOneGroup} from 'rc-tween-one';
import {PlusOutlined} from '@ant-design/icons';
import ModalWithFormGroup from '../modalWithFormGroup'
import {ExperienceProps, EducationProps} from '../../index';
import styled from 'styled-components';
import {useHistory} from 'react-router-dom';
import moment, {Moment} from 'moment';

const {REACT_APP_SERVER_URL} = process.env;
const dateFormat = 'YYYY/MM/DD';

const StyledContainer = styled.div`
    width: 600px;
`

const StyledDiv = styled.div`
    padding: 10px;
    border: 1px #8080804d dashed; 
    margin: 10px 0;   
`

type Props = {
    formValues: Store;
    visible: number;
    onSetVisible: (visible: number) => void;
}

type ResumeInfo = {
    name: string;
    fakultet?: string;
    position?: string;
    address: string;
    from_date: Moment;
    to_date: Moment;
}


function SecondStep({formValues, visible, onSetVisible}: Props) {
    const [tags, setTags] = useState<string[]>([]);
    const [inputVisible, setInputVisible] = useState<boolean>(false);
    const [inputValue, setInputValue] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(false);
    const [experienceModalVisible, setExperienceModalVisible] = useState<boolean>(false);
    const [educationModalVisible, setEducationModalVisible] = useState<boolean>(false);
    const [educationList, setEducationList] = useState<ResumeInfo[]>([]);
    const [experienceList, setExperienceList] = useState<ResumeInfo[]>([]);
    const history = useHistory();


    const handleConfirm = async () => {
        setLoading(true);

        if (tags.length === 0 || educationList.length === 0 || experienceList.length === 0) {
            setLoading(false);
            if (!tags.length) {
                message.error('One Skill is required');
            }

            if (!educationList.length) {
                message.error('One Education is required');
            }

            if (!experienceList.length) {
                message.error('One Experience is required');
            }

            return;
        }

        const formData = new FormData();
        Object.keys(formValues).forEach(key => {
            if (key !== 'file') {
                formData.append(key, formValues[key] || '');
            } else {
                formData.append('file', formValues['file'][0].originFileObj)
            }

        });

        experienceList.forEach((experience, index) => {
            formData.append(`experiences[${index}][name]`, experience.name);
            formData.append(`experiences[${index}][position]`, experience?.position || '');
            formData.append(`experiences[${index}][address]`, experience?.address || '');
            formData.append(`experiences[${index}][from_date]`, experience.from_date.format('YYYY-MM-DD'));
            formData.append(`experiences[${index}][to_date]`, experience.to_date.format('YYYY-MM-DD'));
        });
        educationList.forEach((education, index) => {
            formData.append(`educations[${index}][name]`, education.name);
            formData.append(`educations[${index}][fakultet]`, education?.fakultet || '');
            formData.append(`educations[${index}][address]`, education?.address || '');
            formData.append(`educations[${index}][from_date]`, education.from_date.format('YYYY-MM-DD'));
            formData.append(`educations[${index}][to_date]`, education.to_date.format('YYYY-MM-DD'));
        });
        tags.forEach((tag, index) => {
            formData.append(`skills[${index}][name]`, tag);
        });

        // formData.append('experiences', JSON.stringify(experienceList));
        // formData.append('skills', JSON.stringify(tags));
        // formData.append('educations', JSON.stringify(educationList));


        try {
            const response = await fetch(REACT_APP_SERVER_URL + '/resume', {
                method: 'POST',
                body: formData,
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('access_token')}`
                }
            });

            const result = await response.json();

            setLoading(false);

            if (response.status !== 200) {
                message.error(result.message);
                return;
            }
            history.push("/");
        } catch (error) {
            setLoading(false);
            message.error('Something went wrong');
        }
    };


    const handleClose = (removedTag: string) => {
        const newTags = tags.filter(tag => tag !== removedTag);
        setTags(newTags);
    };

    const showInput = () => {
        setInputVisible(true)
    };

    const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
        setInputValue(e.target.value)
    };

    const handleInputConfirm = () => {
        let newTags = [...tags];

        if (inputValue && tags.indexOf(inputValue) === -1) {
            newTags = [...tags, inputValue];
        }
        setTags(newTags);
        setInputValue('');
        setInputVisible(false);
    };

    const addEducation = (values: Store) => {
        const education: ResumeInfo = {
            name: values.name,
            fakultet: values.fakultet,
            address: values.address,
            from_date: values.date[0],
            to_date: values.date[1],
        };
        const newEducationList = [...educationList];
        newEducationList.push(education);
        setEducationList(newEducationList);
        setEducationModalVisible(false);
    };

    const addExperience = (values: Store) => {
        const experience: ResumeInfo = {
            name: values.name,
            position: values.position,
            address: values.address,
            from_date: values.date[0],
            to_date: values.date[1],
        };
        const newExperienceList = [...experienceList];
        newExperienceList.push(experience);
        setExperienceList(newExperienceList);
        setExperienceModalVisible(false);
    };

    const removeItemFromArray = (type: string, index: number) => {
        if (type === 'experience') {
            const newExperienceList = experienceList.filter((v, itemIndex) => itemIndex !== index);

            setExperienceList(newExperienceList)
        } else {
            const newEducationList = educationList.filter((v, itemIndex) => itemIndex !== index);

            setEducationList(newEducationList)
        }
    }

    if (visible !== VisibleScreen.second) {
        return null;
    }

    const RenderTag = (tag: string) => {
        const tagElem = (
            <Tag
                color="#f50"
                closable
                onClose={(e: ChangeEvent) => {
                    e.preventDefault();
                    handleClose(tag);
                }}
            >
                {tag}
            </Tag>
        );
        return (
            <span key={tag} style={{display: 'inline-block'}}>
              {tagElem}
            </span>
        );
    };

    const RenderInfo = (item: ResumeInfo, index: number, type: string) => (
        <div key={index}>
            <Row justify="space-between">
                <Col>
                    <Space>
                        <div>{item.from_date.format(dateFormat)} -</div>
                        <div>{item.to_date.format(dateFormat)}</div>
                    </Space>
                </Col>
                <Col>
                    <Space>
                        <div>{type === 'experience' ? 'Company' : 'University'}: {item.name}</div>
                        <div>{type === 'experience' ? 'Position' : 'Faculty'}: {item.position || item.fakultet}</div>
                        <Button danger type="text" onClick={() => removeItemFromArray(type, index)}>X</Button>
                    </Space>
                </Col>

            </Row>
        </div>
    );

    return (
        <StyledContainer>
            <StyledDiv>
                <div>
                    {inputVisible && (
                        <Input
                            type="text"
                            style={{width: 150}}
                            value={inputValue}
                            onChange={handleInputChange}
                            onBlur={handleInputConfirm}
                            onPressEnter={handleInputConfirm}
                        />
                    )}
                    {tags.length < 10 && !inputVisible && (
                        <Button
                            type="primary"
                            onClick={showInput} className="site-tag-plus">
                            <PlusOutlined/> Add Skill
                        </Button>
                    )}
                </div>
                {!!tags.length && <div style={{paddingTop: 10}}>
                  <TweenOneGroup
                    enter={{
                        scale: 0.8,
                        opacity: 0,
                        type: 'from',
                        duration: 100,
                    }}
                    leave={{opacity: 0, width: 0, scale: 0, duration: 200}}
                    appear={false}
                  >
                      {tags.map(tag => RenderTag(tag))}
                  </TweenOneGroup>
                </div>}
            </StyledDiv>
            <StyledDiv>
                {
                    educationList.length < 10 && (
                        <Button
                            onClick={() => setEducationModalVisible(true)}
                            type="primary"
                        >
                            <PlusOutlined/> Add Education
                        </Button>
                    )
                }
                <ModalWithFormGroup
                    visible={educationModalVisible}
                    onClose={() => setEducationModalVisible(false)}
                    ModalProps={EducationProps}
                    onFinish={addEducation}
                />

                {educationList &&
                educationList.map((item, index) => RenderInfo(item, index, EducationProps.type))
                }
            </StyledDiv>

            <StyledDiv>
                {
                    experienceList.length < 10 && (
                        <Button
                            onClick={() => setExperienceModalVisible(true)}
                            type="primary"
                        >
                            <PlusOutlined/> Add Experience
                        </Button>
                    )
                }
                <ModalWithFormGroup
                    visible={experienceModalVisible}
                    onClose={() => setExperienceModalVisible(false)}
                    ModalProps={ExperienceProps}
                    onFinish={addExperience}
                />

                {experienceList &&
                experienceList.map((item, index) => RenderInfo(item, index, ExperienceProps.type))
                }
            </StyledDiv>

            <Row justify="space-between">
                <Col>
                    <Button
                        onClick={() => onSetVisible(VisibleScreen.first)}
                    >
                        Previous
                    </Button>
                </Col>
                <Col>
                    <Button
                        loading={loading}
                        onClick={handleConfirm}
                    >
                        Confirm
                    </Button>

                </Col>
            </Row>
        </StyledContainer>
    );
}

export default SecondStep;

import React from "react";
import {Button, Space} from 'antd';
import {Link} from "react-router-dom";
import {isLoggedIn} from '../../reducers/auth';
import {useSelector} from "react-redux";

function Home() {
    const isAuthenticated = useSelector(isLoggedIn);
    return (
        <Space>
            {!isAuthenticated ?
                <>
                    <Button type="primary">
                        <Link to="/login">Login</Link>
                    </Button>

                    <Button type="primary">
                        <Link to="/register">Register</Link>
                    </Button>
                </> :
                <>
                    <Button>
                        <Link to="/add-resume">Create Resume</Link>
                    </Button>
                    <Button>
                        <Link to="/all-resumes">My Resumes</Link>
                    </Button>
                </>
            }
        </Space>
    )
}

export default Home;

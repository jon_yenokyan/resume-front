import React, {useState} from 'react';

import {Form, Input, Button, message} from 'antd';
import {Store} from 'rc-field-form/lib/interface';

import {emailPattern} from '../../helpers/auth';
import {useHistory} from 'react-router-dom';
import {useDispatch} from 'react-redux';
import {setIsLoggedIn} from '../../reducers/auth';

const {REACT_APP_SERVER_URL} = process.env;

const layout = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
};
const tailLayout = {
    wrapperCol: {offset: 8, span: 16},
};

function Login() {
    const dispatch = useDispatch();
    const [loading, setLoading] = useState<boolean>(false);

    const history = useHistory();
    const onFinish = async (values: Store) => {

        setLoading(true);
        const {email, password} = values;
        const formData = new FormData();
        formData.append('email', email);
        formData.append('password', password);

        try {
            const response = await fetch(REACT_APP_SERVER_URL + '/auth-login', {
                method: 'POST',
                body: formData,
                headers: {
                    'Accept': 'application/json'
                }
            });

            const result = await response.json();
            setLoading(false);

            if (response.status !== 200) {
                message.error(result.message);
                return;
            }
            localStorage.setItem('access_token', result.access_token);
            dispatch(setIsLoggedIn(!!result.access_token));
            history.push("/");
        } catch (error) {
            setLoading(false);
            message.error('Something went wrong');
        }
    };

    return (
        <Form
            {...layout}
            name="basic"
            onFinish={onFinish}
        >
            <Form.Item
                label="Email"
                name="email"
                rules={[
                    {required: true, message: 'Please input your email!'},
                    {
                        pattern: emailPattern,
                        message: 'Invalid email.'
                    },
                ]}>
                <Input/>
            </Form.Item>
            <Form.Item
                label="Password"
                name="password"
                rules={[
                    {required: true, message: 'Please input your password!'},
                    {min: 6, message: 'Minimum 6 characters.'},
                ]}>
                <Input.Password/>
            </Form.Item>
            <Form.Item {...tailLayout}>
                <Button type="primary" htmlType="submit" loading={loading}>
                    Submit
                </Button>
            </Form.Item>
        </Form>
    );
}

export default Login;

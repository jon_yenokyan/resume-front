import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {RootState} from '../store';

type Auth = {
    isLoggedIn: boolean;
}

const initialState: Auth = {
    isLoggedIn: false,
};

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        setIsLoggedIn: (state, action: PayloadAction<boolean>) => {
            state.isLoggedIn = action.payload;
        }
    }
});

export const {setIsLoggedIn} = authSlice.actions;

export const isLoggedIn = (state: RootState) => state.auth.isLoggedIn;

export default authSlice.reducer;
